# Copyright (c) 2024 Telefónica Innovación Digital
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Alejandro Muñiz - Autor
#     Luis M. Contreras - Coautor

'''
This code is elaborated as the mNCC NBi for metrics exposure.
'''
import ast
from time import sleep
import json
import argparse
from datetime import datetime
import logging
import requests
from kubernetes import config, client
import pika

TEST_MODE = 0
CLUSTER_ID = "CLUSTER-1"
MAX_FILE_SIZE = 1 * 1024 * 1024  # 1MB


class Nemex:
    '''
    Class that represents the NeMeX component.
    Atributes:
        Delay: updating frequency.
        Exchange: RabbitMQ exchange used for pushing metrics.
        Key: Queue key to identify where the metrics pushing.
        rmq_credentials: User:Password to access RMQ.
        Environment variables: K8S, Logger and Pika atributes used.
        filename: archive where datasets are created.
        timestamp: Last Update timestamp.
    '''

    def __init__(self, update_delay, exchange, key, user, pswd):
        """
        Class used to proccess the different sources of information in mncc.
        Imput:
            Metrics: List of collected metrics and how are they mapped in the CRD.
            update_delay: expected periodicity between updates.
            ip:port: Peer used to access to internal prometheus where metrics will be colllected.
        Paramethers:
            self.url.pmth = url where the internal prometheus is launched.
            self.metrics = param with the metrics received.
            self.alto = ALTO demon that compilates network metrics.
        """

        # Recording the required arguments
        self.delay = int(update_delay)
        self.exchange = exchange
        self.key = key
        self.rmq_credentials = {"user": user, "password": pswd}

        # Configuring the environment variables.
        config.load_incluster_config()
        self.k8s = client.CoreV1Api()
        self.k8s_client = client.ApiClient()
        self.custom_api = client.CustomObjectsApi(self.k8s_client)
        self.probes = []
        pods = self.k8s.list_namespaced_pod(
            namespace="nemo-net", label_selector="app=network-probe"
        )
        for pod in pods.items:
            url = "http://" + str(pod.status.pod_ip) + ":5001"
            self.probes.append(url)

        # Configuring Logs and datasets
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        timestamp = int(datetime.now().timestamp())
        self.filename = f"datasets/metric-{timestamp}.json"
        with open(self.filename, "w", encoding='utf-8') as f:
            f.write("[]")

    def rabbitmq_sender(self, r_exchange, routing_key, message, user, password):
        '''
        Function dedicated to pushing metrics to Rabbit MQ.
        '''
        print("Connecting Rabbit MQ.")
        try:
            credentials = pika.PlainCredentials(user, password)
            connection = pika.BlockingConnection(
                pika.ConnectionParameters(
                    host="132.227.122.23", port=30403, credentials=credentials
                )
            )
            channel = connection.channel()

            # Exchange:     mncc
            # Routing key:  costs-map-vectorial
            # Body: el mensaje.
            channel.basic_publish(
                exchange=r_exchange, routing_key=routing_key, body=message
            )
            self.logger.info(" [x] Sent %s:%s", routing_key, message)
            self.guardar_metricas_json(message)
        except pika.exceptions.IncompatibleProtocolError as e:
            self.logger.error("Incompatible protocol error: %s", e)
            self.logger.info(
                "User: %s\tExchanger: %s\tKey: %s", user, r_exchange, routing_key
            )
        except pika.exceptions.AMQPConnectionError as e:
            self.logger.error("Connection error: %s", e)
            self.logger.info(
                "User: %s\tExchanger: %s\tKey: %s\tPassword: %s", user, r_exchange, routing_key, password
            )
        except Exception as e:
            self.logger.error("An error occurred: %s", e)
            self.logger.info(
                "User: %s\tExchanger: %s\tKey: %s", user, r_exchange, routing_key
            )
        finally:
            if "connection" in locals() and connection.is_open:
                connection.close()

    ###########################
    # Metric digest functions #
    ###########################

    def guardar_metricas_json(self, metricas):
        """
        Saves the metrics in a output file. 
        If the file is not available (no permissions, wrong name, ...) it does nothing.
        Imput:
            metricas: list of metrics received. Expected in a JSON format.
        """
        if not TEST_MODE:
            with open(self.filename, "r+", encoding='utf-8') as f:
                data = json.load(f)
                data.append(metricas)
                file_size = len(json.dumps(data).encode("utf-8"))
                # Checking the max size of the file.
                if file_size >= MAX_FILE_SIZE:
                    logging.warning(
                        "File %s passed 1MB, deleting oldest entries.", self.filename
                    )
                    # Droping oldest lines.
                    while file_size >= MAX_FILE_SIZE and len(data) > 1:
                        data.pop(0)
                        file_size = len(json.dumps(data).encode("utf-8"))
                f.seek(0)
                json.dump(data, f, indent=4)
                f.truncate()
            self.logger.info("Metrics saved in: %s", self.filename)
        else:
            self.logger.debug("Metrics not saved.")

    def get_underlay_metrics(self, underlay_url="http://10.103.9.188:8090/metrics"):
        """
        This function extracts the metrics from underlay metrics API.
        It is expected to remove/update this method in future versions once we have
        a final datamodel for underlay metrics.
        Imput:
            underlay_url: url to be read.
        Output:
            Raw data from underlay monitoring tools.
        """
        if not TEST_MODE:
            resp = requests.get(underlay_url,timeout=10)
            response = str(resp.text)
        else:
            response = self.read_test_sources("get_underlay_metrics_format")
        data = []
        for req in response.split("\n"):
            if len(req) < 1:
                continue
            if req[0] != "#":
                data.append(req)
        return data

    def process_prometheus_umetrics(self, data):
        """
        Proccess the prometheus underlay metrics.
        To be updated once all metrics are integrated in a common format.
        Imput:
            data: raw information obtained from network state monitoring module.
        Output:
            nodes and links dictionaries with its respective metrics.
        """
        metrics = {
            "network_metric_latency": "latency",
            "network_metric_throughput": "uBandWidthBits",
            "network_metric_packet_loss": "packet-loss",
            "network_metric_link_energy": "link-energy",
            "network_metric_link_failure": "link-failure",
        }
        datos = {}
        nodos = {}
        for metrica in data:
            ident, valor = metrica.split(" ")
            if "{" not in ident:
                self.logger.debug("Nos saltamos la iteracion:\t%s", str(metrica))
                continue
            met, pseudo_json = ident.split("{")
            if met not in metrics.keys():
                self.logger.debug("Nos saltamos la iteracion:\t%s", str(metrica))
                continue
            jason = '{"' + pseudo_json.replace("=", '":').replace(",", ',"')
            self.logger.debug("JASON:\t%s", str(jason))
            labels = json.loads(jason)
            src = labels["current_node"]
            dst = labels["remote_node"]

            link = "link_" + src + "_" + dst
            if src not in nodos.keys():
                nodos[src] = {"uNodeBandWidth": "0"}
            if dst not in nodos.keys():
                nodos[dst] = {"uNodeBandWidth": "0"}

            if link not in datos:
                datos[link] = {}
            if met in metrics:
                datos[link][metrics[met]] = str(valor)

            if met == "network_metric_throughput":
                nodos[src]["uNodeBandWidth"] = str(
                    float(nodos[src]["uNodeBandWidth"]) + float(valor)
                )
        self.logger.debug("DATOS:\n%s", str(datos))
        return nodos, datos

    def read_test_sources(self, file):
        '''
        Function oriented to read files.
        '''
        data = {}
        with open(file, "r", encoding='utf-8') as f:
            data = json.load(f)
        return data

    ###############################
    # Exposure-oriented functions #
    ###############################

    def get_kubernetes_node_ips(self):
        """
        Connects to the Kubernetes cluster and retrieves the IPs of the nodes.
        """

        # Retrieve node information
        node_ips = {}
        nodes = self.k8s.list_node().items
        for node in nodes:
            node_name = node.metadata.name
            # Extract IP addresses, preferring InternalIP
            addresses = node.status.addresses
            for address in addresses:
                if address.type == "InternalIP":
                    node_ips[node_name] = address.address
                elif address.type == "ExternalIP":
                    node_ips[node_name] = address.address

        return node_ips

    def create_network_metrics(self, nodos, links):
        '''
        Formats the information collected into the expected output.
        '''
        # Fixed metadata structure
        metadata = {
            "name": "mncc-sample",
            "cluster": CLUSTER_ID,
            "timestamp": datetime.timestamp(datetime.now()),
            "metrics": [
                {
                    "name": "link-failure",
                    "type": "integer",
                    "metric": "failures per hour",
                },
                {"name": "link-energy", "type": "integer", "metric": "I'm not sure"},
                {
                    "name": "packet-loss",
                    "type": "integer",
                    "metric": "packets per hour",
                },
                {"name": "bandwidth", "type": "float", "metric": "bits"},
                {"name": "latency", "type": "float", "metric": "nanoseconds"},
            ],
        }

        # Get Kubernetes node IPs
        node_ips = self.get_kubernetes_node_ips()

        # Build the list of nodes
        nodes = [
            {"name": node_name, "ip": node_ips.get(node_name, "0.0.0.0")}
            for node_name in nodos.keys()
        ]

        # Build the list of links
        links_list = []
        for link_name, link_data in links.items():
            # Split the link name to get the source and destination
            source, destination = link_name.replace("link_", "").split("_")

            link_entry = {
                "name": f"{source}_{destination}",
                "source": source,
                "destination": destination,
                "metrics": {
                    "link-failure": int(
                        float(link_data["link-failure"])
                    ),  # Converted to integer
                    "link-energy": int(
                        float(link_data["link-energy"])
                    ),  # Converted to integer
                    "packet-loss": int(
                        float(link_data["packet-loss"])
                    ),  # Converted to integer
                    "bandwidth": float(link_data["uBandWidthBits"]),  # Float value
                    "latency": float(link_data["latency"]),  # Float value
                },
            }

            links_list.append(link_entry)

        # Final JSON structure
        result = {
            "metadata": metadata,
            "network-metrics": {"nodes": nodes, "links": links_list}
        }

        # Convert to JSON string (for demonstration purposes)
        return result

    #############################################################
    # Main functionalities functions / functions to be executed #
    #############################################################

    def manage_topology_updates(self):
        """
        Main function in Nemex. It reads the information from all available sources
        and integrates them all in a single CR that later it is expossed.
        To be updated in version 2 to accept custom monitoring requests.
        """
        test = 0
        while not test:
            # Reloading lists.
            nodos = {}
            metrics = {}

            ## Metricas compute underlay
            mets = {}
            met = []
            self.logger.debug("Reading metrics.")
            if TEST_MODE:
                met = ast.literal_eval(self.read_test_sources("get_underlay_metrics_format"))
            else:
                for uri in self.probes:
                    met.extend(self.get_underlay_metrics(uri))
            mets = self.process_prometheus_umetrics(met)
            nodos, metrics = mets

            self.logger.debug("\nUnderlay metrics loaded OK:")
            self.logger.debug("Nodos:\n%s", str(nodos))
            self.logger.debug("Links:\n%s", str(metrics))

            resultado = self.create_network_metrics(nodos, metrics)

            self.logger.debug("RESULTADOS:\n%s", str(resultado))
            self.rabbitmq_sender(
                self.exchange,
                self.key,
                str(resultado),
                self.rmq_credentials["user"],
                self.rmq_credentials["password"],
            )
            sleep(self.delay)

            if TEST_MODE:
                test = 1


if __name__ == "__main__":

    ##################
    # Execution code #
    ##################

    parser = argparse.ArgumentParser(description="NeMeX argument loader.")

    # If the argument is defined, it saves it, if not it saves the default value.
    parser.add_argument(
        "--delay", type=int, default=3600, help="Time between updates (default: 3600)"
    )
    parser.add_argument(
        "--cluster",
        type=str,
        default="cluster-1",
        help="Cluster id (default: 'cluster-1')",
    )
    parser.add_argument(
        "--exchange",
        type=str,
        default="",
        help="Exchange used int the Rabbit MQ (default: '')",
    )
    parser.add_argument(
        "--key",
        type=str,
        default="costs-map",
        help="Key used in RabitMQ (default: 'costs-map')",
    )
    parser.add_argument(
        "--user",
        type=str,
        default="nemo-user",
        help="Nemo user for pushing RabitMQ messages (default: 'nemo-user')",
    )
    parser.add_argument(
        "--passwd",
        type=str,
        default="1234",
        help="Password to push RabitMQ messages (default: '1234')",
    )



    # Parsear los argumentos
    args = parser.parse_args()

    CLUSTER_ID = args.cluster
    logging.info("Launching NeMeX.")
    logging.info("Arguments:\n%d\t%s\t%s", args.delay, args.exchange, args.key)
    nemex = Nemex(args.delay, args.exchange, args.key, args.user, args.passwd)
    nemex.manage_topology_updates()
