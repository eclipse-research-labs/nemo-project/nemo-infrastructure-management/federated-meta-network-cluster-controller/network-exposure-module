# Network Exposure module
## Table of contents
1.  [General description](#general-description)
2.  [Needed Arguments](##needed-arguments)
3.  [List of files](#list-of-files)
4.  [Ecosystem Preparation and NeMeX deployment](#ecosystem-preparation-and-nemex-deployment)
7.  [Versions](#versions)
8.  [Authors and acknowledgment](#authors-and-acknowledgment)
9.  [License](#license)
10. [Project status](#project-status)
11. [How to colaborate](#how-to-colaborate)

## General description
Directory created to include the code related with the mNCC Network Metrics Exposure. This component will be based on Kubernetes native tools and an ALTO implementation. As it is described in D2.2, Network Metrics Exposure Module is the component that will work as a North-bound interface (NBI) for mNCC. It will interact with both CF-DRL and MO to send them the network performance metrics for each cluster in NEMO. 

The Network Metrics Exposure Module (NeMeX) has two main functionalities: On the one side it will be the component that works as a topology view integrator, receiving information from multiple sources and integrating it in a single picture. On the other side, it is the subcomponent in charge of exposing the network topology emtrics for each cluster and to send the performance information to the Meta-Orchestrator via Rabbit MQ. 

To be able to realize this assignment, NeMeX will use a python engine to integrate the different metrics in a single view, notifiying about the IP nodes connected and the performance metrics that will be further explained. 
 
The first step followed by NeMeX sub-component once it is deployed will be to stablish connection with Prometheus pod. As mNCC will be launched all together, each IP:Port peer will be extracted from the Network Probes deployed. These probes are a component of mNCC and therefore will be tatic declared, eventhought they can be modiffied in case the probes are used in other scenarios. Once connection with the mNCC Prometheus is stablished, it will start reading metrics, mapping the entries’ names obtained from Prometheus with the metrics to expose. These values will be integrated into a single view that will be related according with the Kubernetes node name. The matching with the IPs will be possible with the Nodes information.
	
This process will be realized periodically each X seconds (need to be defined by the user; default time is 3600s), and once the values are updated, they will be sent to the Rabbit MQ queue that will be read by the MO and CF-DRL.
		 
Finally, the requirements defined for this module are the following ones:
+ Device capabilities: no CPU or RAM dependencies. At least one network card with Internet connection.
+ Software dependencies: Kubernetes installed (no version dependencies). The image created with the code has other dependencies, but, as the code will be deployed into a docker image, those dependencies will not be needed in the host device.

## Needed Arguments
```
--delay: Time between updates (default: 3600)
--cluster: Cluster id (default: 'cluster-1')
--exchange: Exchange used int the Rabbit MQ (default: '')
--key: Key used in RabitMQ (default: 'costs-map')
--user: Nemo user for pushing RabitMQ messages (default: 'nemo-user')
--passwd: Password to push RabitMQ messages (default: '1234')
```

## List of files

+ kuberfiles/: folder with the resources to be deployed:
	+ 00_namespace.yaml: definition of nemo-mncc namespace.
	+ 01_secret.yaml: nemex-rmq-credentials credentials (not included in the git).
	+ 02_nemex-deployment.yaml: NeMeX K8S definition, it includes the Deployment, ServiceAccount, ClusterRole and ClusterRoleBinding.
+ tests/: list of examples for internal request. Usefull for testing.
+ Dockerfile: Definition to dockerize NeMeX module.
+ NeMeX.py: Core code for this component.
+ datasets/: Folder where metrics will be saved. Each time a NeMeX instance is launched it will be saved in a single file. Max of 1MB for each file.
+ requirements.txt: Document with Python dependencies.

## Ecosystem preparation and NeMeX deployment

Before deploy the NeMeX sub-component, it is needed to deploy the underlay monitoring sub-components available in the mNCC component folder underlay-network-probe.

Then, it is needed to clone the NeMeX repository:
```
kubectl apply -f https://gitlab.eclipse.org/eclipse-research-labs/nemo-project/nemo-infrastructure-management/federated-meta-network-cluster-controller/network-exposure-module/-/tree/main/kubefiles
```


## Versions

v1.0: Full functionality but without Rabbit MQ exposing.
v2.0: Full functionality with RabbitMQ.
v2.1: Added parameters as arguments and droping credentials from code. Improvements in the Manifest (resources limits, secrets, new execution order).
v2.2: Added logging process and datasets.

## Authors and acknowledgment
For any doubt, please contact Alejandro Muñiz Da Costa: alejandro.muniz@telefonica.com

## License
© 2024 Telefónica Innovación Digital

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## Project status

Currently we are on a multi-cluster integration state.

## How to Colaborate


- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.eclipse.org/eclipse-research-labs/nemo-project/nemo-infrastructure-management/federated-meta-network-cluster-controller/network-exposure-module.git
git branch -M main
git push -uf origin main
```

### Integrate with your tools
- [ ] [Set up project integrations](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/network-management-and-adaptation-netma/network-exposure/-/settings/integrations)

### Test and Deploy
Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***


